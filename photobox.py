#!/usr/bin/env python3

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

from PIL import Image, ImageTk

import picamera
import time
import datetime
import shutil
import smtplib
import os
from tkinter import messagebox as tkMessageBox
import subprocess as sub

bilder = "katharina.png"

root = Tk()
root.geometry("1024x600")
root.title("Katharinas Photobox")
root.attributes("-fullscreen", True)
root.resizable(0, 0)

camera = picamera.PiCamera()
camera.awb_mode = 'auto'
camera.brightness = 55
data=""

class fotomail:
    def __init__(self, master):
        #picture frame
        self.etPicture = Label(root, font=('arial narrow', 14, 'normal'))
        self.etPicture.grid(row=0, rowspan=25, column=3, columnspan=4, padx=0, pady=90, sticky=NSEW)

        #text label
        self.textBox = Text(root, height=1, width=1, relief=SUNKEN, font=('arial narrow', 10, 'normal'), bg="green",
                            fg="white")
        self.textBox.grid(row=4, rowspan=2, column=0, padx=3, pady=2, sticky=NSEW)
        self.textBox.insert(END, "READY")

        self.textBox1 = Text(root, height=1, width=1, relief=SUNKEN, font=('arial narrow', 10, 'normal'), bg="green",
                             fg="white")
        self.textBox1.grid(row=4, rowspan=2, column=1, padx=3, pady=2, sticky=NSEW)

        #snap button
        self.botshoot = Button(root, width=18, height=3, font=('arial narrow', 19, 'normal'), text="[*o ] SNAP!", bg="#5e3e1b",
                               activebackground="#00dfdf", fg='#ffffff')
        self.botshoot.grid(row=6, rowspan=1, column=0, columnspan=2, ipady=15, pady=2, padx=2, sticky=NSEW)
        self.botshoot.configure(command=self.capture_image)
        
        #button prnt
        self.botQuit = Button(root, height=3, font=('arial', 18, 'normal'), text="SCHLIEßEN", bg="#36353d", fg="#ffffff",activebackground="#00dfdf")
        self.botQuit.grid(row=16, rowspan=1, column=0, columnspan=2, pady=0, sticky=NSEW)
        self.botQuit.configure(command=self.closewindow)

        #quit button 
        #self.botQuit = Button(root, heigh=1, font=('arial', 18, 'normal'), text="QUIT", activebackground="#00dfdf")
        #self.botQuit.grid(row=16, rowspan=2, column=0, columnspan=2, pady=0, sticky=NSEW)
        #self.botQuit.configure(command=self.closewindow)

        self.showImg()
        
    def capture_image(self):
        global bilder
        global texte
        data = time.strftime("%Y-%b-%d_(%H%M%S)")
        texte = "picture take:" + data
        camera.rotation = 180
        camera.start_preview()
        time.sleep(3)
        camera.capture('%s.jpg' % data, resize=(1920, 1080))
        camera.stop_preview()
        time.sleep(1)
        self.textBox.delete("1.0", END)
        self.textBox.insert(END,'%s' % data)
        self.textBox1.delete("1.0", END)
        self.textBox1.insert(END,"   CAPTURED")
        bilder = '%s.jpg' % data 
        self.showImg()
        time.sleep(1)
        sub.run(["lp","-o", "media=A6", "/home/pi/Desktop/photobox/%s.jpg" % data])
        
        
    def showImg(self):
        image = Image.open(bilder)
        image = image.resize((750,422),Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(image)
        
        self.etPicture.configure(image=photo)
        self.etPicture.image=photo
        root.update_idletasks()


  

 
    
        
            # -------------------------------------------BUTTON QUIT

    def closewindow(self):
        
        root.destroy()
    

fotomail(root)
root.mainloop()
