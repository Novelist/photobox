# photobox

Photobox is a self-made photo booth box for family and friends. The setup is using the following devices and hardware:

  - Raspberry Pi Model A+
  - Waveshare 7" HDMI LCD (C)
  - Raspberry Pi Camera v2
  - RGB LED HAT 

# Setup Waveshare HDMI

If you add the screen for the first time, please follow the instructions below:

- Add the following lines to the **config.txt** file of your Raspbian / Ubuntu image (preferably on another device):
```
max_usb_current=1
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt 1024 600 60 6 0 0 0
hdmi_drive=1
```
**Please note: Once you have set this code in your config-file, you can only use this display as the default one.**
- Connect the touch port of the display (micro USB) to your Raspberry Pi
- Connect the HDMI port of the display to your Raspberry Pi
- Switch on the backlight switch
- Start your Raspberry Pi and Go!

# Setup Raspberry Pi Camera v2

Once you have attached the Raspberry Pi Camera to the slot on the pi, you first have to check, if the camera capturing is enabled in the Raspberry Pi Configurations. If you have some troubles after setting up the camera, have a look at the troubleshooting here: https://www.raspberrypi.org/documentation/raspbian/applications/camera.md

